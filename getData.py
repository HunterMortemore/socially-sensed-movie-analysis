#!/usr/local/anaconda3/bin/python3.7

# getData.py
# Script to retrieve data from database

# Imports
import pickle as pkl
import databaseClass as db
import sys

# Helper Functions
def usage(status=0):
    print("Usage: ./getData.py MovieTitle")
    sys.exit(status)

# Main Execution
if __name__ == '__main__':

    # Parse CLA for movie title
    if len(sys.argv) != 2:
        usage()
    if sys.argv[1] == '-h':
        usage()
    else:
        movieTitle = sys.argv[1]

    # load in database
    movies = pkl.load(open('./Database/database.pkl', 'rb'))
    top5 = pkl.load(open('./Database/top5.pkl', 'rb'))

    # get movie data
    if movieTitle in movies:
        score = movies[movieTitle][0]
        tweets = movies[movieTitle][1]
    else: # movie not in database, get data
        score = -1
        tweets = []

    print(score)
    for i, rank in enumerate(tweets):
        print(i + 1)
        for tweet in rank:
            print(tweet)

    for movie in top5:
        print(movie[0], movie[1])

    # exit success
    sys.exit(0)
