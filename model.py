#!/usr/local/anaconda3/bin/python3.7

from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer
import pickle 
import re

class Tweet_Rater():

	def __init__(self):
		f = open("training/all.txt")
		tweets = list()
		labels = list()

		# Get tweets and labels
		while True:
			sentence = f.readline().rstrip()
			label = f.readline().rstrip()

			if not label: break
			tweets.append(" ".join(re.findall(r"[\w]+|[^\s\w]", sentence.lower())))
			labels.append(int(label))

		self.vectorizer = CountVectorizer(min_df=0, lowercase=True)
		self.vectorizer.fit(tweets)


		try: 
			fileObject = open("model.pkl",'rb')
			print("Loading up model, from file.....")
			self.model = pickle.load(fileObject)
			
		except FileNotFoundError:	

			#Create a Gaussian Classifier
			gnb = GaussianNB()

			#Create a Gaussian Classifier
			print("Creating Gaussian Classifier.....")
			self.model = LogisticRegression(random_state=0, solver='lbfgs',multi_class='multinomial', max_iter=200, class_weight='balanced')
			#Train the model using the training set
			print("Training model with "+ str(len(labels)) + " number of labels.....")
			self.model.fit(self.vectorizer.transform(tweets).toarray(), labels)

			# Save a pickle file to not have to constantly save
			print("Saving generated model, to a file.....")
			fileObject = open("model.pkl",'wb') 
			pickle.dump(self.model,fileObject)   
			fileObject.close()


	def predict(self, sentence):
		test = sentence.rstrip().lower()
		predicted= self.model.predict( self.vectorizer.transform( [" ".join(re.findall(r"[\w]+|[^\s\w]", test))] ).toarray() )
		print(test)
		print(predicted[0])
		return predicted[0]
	
	def confidence(self, sentence):
		test = sentence.rstrip().lower()
		test = [" ".join(re.findall(r"[\w]+|[^\s\w]", test))] 
		print(self.model.predict_proba(self.vectorizer.transform(test).toarray()))
		return(self.model.predict_proba(self.vectorizer.transform(test).toarray()))
	

if __name__ == "__main__":

	myRater = Tweet_Rater()
	testFile = open("endgame.txt")
	myRater.predict("I ate movie title for breakfast today")
	myRater.confidence("I ate movie title for breakfast today")