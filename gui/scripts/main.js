// Javascript Backend

// Grabbing main document elements
searchBar = document.getElementById("searchBar")
searchButton = document.getElementById("searchButton")
quoteButton = document.getElementById("quoteButton")

// Handler for ourScore elements
twitterScoreDisplay = document.getElementById("twitterScoreDisplay")
star1 = document.getElementById("star1")
star2 = document.getElementById("star2")
star3 = document.getElementById("star3")
star4 = document.getElementById("star4")
star5 = document.getElementById("star5")
updateOurScore = function(score) {
    twitterScoreDisplay.innerHTML = score
    star1.style.display = "block"
    star2.style.display = "block"
    star3.style.display = "block"
    star4.style.display = "block"
    star5.style.display = "block"
    switch(parseInt(score)) {
        case 1:
            star2.style.display = "none"
        case 2:
            star3.style.display = "none"
        case 3:
            star4.style.display = "none"
        case 4:
            star5.style.display = "none"
    }
}

// Handler for recommendations elements
rec1 = document.getElementById("rec1")
rec2 = document.getElementById("rec2")
rec3 = document.getElementById("rec3")
rec4 = document.getElementById("rec4")
rec5 = document.getElementById("rec5")
updateRecommendations = function(recommendations) {
    rec1.innerHTML = "1: " + recommendations['r1']['score'] + ' - ' + recommendations['r1']['movie']
    rec2.innerHTML = "2: " + recommendations['r2']['score'] + ' - ' + recommendations['r2']['movie']
    rec3.innerHTML = "3: " + recommendations['r3']['score'] + ' - ' + recommendations['r3']['movie']
    rec4.innerHTML = "4: " + recommendations['r4']['score'] + ' - ' + recommendations['r4']['movie']
    rec5.innerHTML = "5: " + recommendations['r5']['score'] + ' - ' + recommendations['r5']['movie']
}

// Handler for quote elements
quoteSelect = document.getElementById("quoteSelect")
quotes = {}
quote1 = document.getElementById("quote1")
quote2 = document.getElementById("quote2")
quote3 = document.getElementById("quote3")
quote4 = document.getElementById("quote4")
quote5 = document.getElementById("quote5")
updateQuotes = function() {
    quoteLevel = quoteSelect.value
    quote1.innerHTML = "\"" + quotes[quoteLevel]['tweet1'] + "\""
    quote2.innerHTML = "\"" + quotes[quoteLevel]['tweet2'] + "\""
    quote3.innerHTML = "\"" + quotes[quoteLevel]['tweet3'] + "\""
    quote4.innerHTML = "\"" + quotes[quoteLevel]['tweet4'] + "\""
    quote5.innerHTML = "\"" + quotes[quoteLevel]['tweet5'] + "\""
}

// Main handler for movie search
submitServerRequest = function() {
    movie = searchBar.value.replace(/\s+/g, '')
    requestURL = 'http://localhost:51000/all/' + movie
    var XHR = new XMLHttpRequest()
    XHR.open('GET', requestURL)
    XHR.onload = function(e) {
        if(XHR.readyState == 4) {
            updateOurScore(JSON.parse(XHR.responseText)['score'])
            updateRecommendations(JSON.parse(XHR.responseText)['recommendations'])
            quotes = JSON.parse(XHR.responseText)['quotes']
            updateQuotes()
        } else {
            console.error(XHR.statusText)
        }
    }
    XHR.send(null)
}

// Assign event handlers
searchButton.onmouseup = submitServerRequest
quoteButton.onmouseup = updateQuotes