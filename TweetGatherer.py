#!/usr/local/anaconda3/bin/python3.7

# TweetGatherer.py
# Generalized tweet gathering script which simply returns tweets containing
# provided keywords.

# Imports

import sys
import tweepy
import string

# Constants

CONSTOK = "CRDQHCfGJxmu68Z2tbBtmfvxU"
CONSTOKSEC = "ZQZ7YEPGTZ7dYupVHXrJx776HK57zwotUj7egMavgupk08TIRK"
ACESTOK = "1091770156961943558-ZtkYCtuDQPZfENWKOWSaOgeLzXAcYm"
ACESTOKSEC = "LfA7fquVMrF1KyzKwfnUK64zjX1SloctRqBiq4Zn5gbPh"


# Helper Functions

def usage(status=0):
    print("Usage: ./TweetGatherer.py flags KEYWORD1 KEYWORD2 KEYWORD3 ...")
    print("Flags: -n NUMBEROFTWEETS")
    sys.exit(status)

def tweetCleanup(tweet, movieName ):
    wordList = list()
    
    for word in tweet.split():
        appendWord = True 
        if "@" in word:
            appendWord = False
        if "http" in word:
            appendWord = False

        if(appendWord):
            wordList.append(word.lower())

    finalTweet = " ".join(wordList).replace(movieName.lower(), 'MOVIE TITLE')
    return(finalTweet)

# Main Execution

if __name__ == '__main__':
    # Prepare initial variables
    numTweets = 500

    # Parse CLA for options
    if len(sys.argv) <= 1:
        usage()
    if sys.argv[1] == '-n':
        numTweets = int(sys.argv[2])
        keys = sys.argv[3:]
    else:
        keys = sys.argv[1:]
    
    # Prepare Tweepy
    auth = tweepy.OAuthHandler(CONSTOK, CONSTOKSEC)
    auth.set_access_token(ACESTOK, ACESTOKSEC)
    api = tweepy.API(auth)

    # Form query string
    query = " OR ".join(keys)

    # Get first 50 tweets that contain keywords
    tweets = tweepy.Cursor(api.search, q=query, lang="en", tweet_mode='extended').items(numTweets)

    # Open file to print to
    f = open("textFiles/" + query + ".txt", "w+")

    # Print tweet text seperated by empty lines
    for tweet in tweets:
        # In order to get full text we check if it is a retweet
        try: 
            tweetText = tweet.retweeted_status.full_text
        except:
            tweetText =tweet.full_text

        cleanedTweet=tweetCleanup(tweetText, query)

        #Determine if tweet is worth printing.....
        if(cleanedTweet and "MOVIE TITLE" in cleanedTweet):
            f.write(cleanedTweet + "\n")
            f.write("\n")

    # Exit success
    f.close()
    sys.exit(0)
