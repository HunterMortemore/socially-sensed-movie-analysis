#!/usr/local/anaconda3/bin/python3.7

# movieClass.py
# The movie class that is used to store data in the database

# Imports
import datetime

class Database():
    def __init__(self):
        self.movies = {}
        self.topFive = []

    def updateList(self, title, score, tweets):
        self.movies[title] = (score, tweets, datetime.datetime.now())

    def updateTopFive(self, title, score):
        # check if movie already in top five
        titles = [movie[0] for movie in self.topFive]
        if title in titles:
            return

        # update top five
        if len(self.topFive) < 5:
            self.topFive.append((title, score))
            self.topFive.sort(key=lambda x: x[1], reverse=True)
        elif score >= self.topFive[-1][1]:
            self.topFive[-1] = (title, score)
            self.topFive.sort(key=lambda x: x[1], reverse=True) 

    def updateDatabase(self, title, score, tweets):
        self.updateList(title, score, tweets)
        self.updateTopFive(title, score)
