#!/usr/local/anaconda3/bin/python3.7

# SentAn.py
# Script to read in Tweet text from a text file and perform sentiment analysis

# Imports
import sys
import model

# Helper Functions
def usage(status=0):
    print("Usage: ./SentAn.py FILENAME")
    sys.exit(status)

# Main Execution
if __name__ == '__main__':

    myRater= model.Tweet_Rater()

    # Parse CLA for filename
    if len(sys.argv) != 2:
        usage()
    if sys.argv[1] == '-h':
        usage()
    else:
        tweetFile = sys.argv[1]

        sumScore = 0
        numTweets = 0
    # Perform Sentiment analysis
    with open(tweetFile, encoding='utf8') as f:
        for tweet in f.readlines():
            # ignore blank lines
            if tweet == '\n':
                continue

            # load in Tweet (just text separated by blank line)
            #blob = TextBlob(tweet)
            #sumScore+=blob.sentiment.polarity
            if(myRater.predict(tweet)!=0):
                sumScore+=myRater.predict(tweet)
                numTweets+=1

            # perform sentiment analysis
            #print(blob.sentiment.polarity, tweet)
    
    print("Average Score: ", sumScore/numTweets )
    # Exit success
    sys.exit(0)
