#!/usr/local/anaconda3/bin/python3.7

# updateData.py
# Script to update an entry in the database

# Imports
import model
import databaseClass as db
import pickle as pkl
import sys
import subprocess

# Helper Functions
def usage(status=0):
    print("Usage: ./updateData.py MovieTitle")
    sys.exit(status)

# Main Execution
if __name__ == '__main__':
    
    # Parse CLA for movie title
    if len(sys.argv) != 2:
        usage()
    if sys.argv[1] == '-h':
        usage()
    else:
        movieTitle = sys.argv[1]

    # get positive/negative tweets
    tweets = []
    for i in range(5):
        tweets.append([])
    rater = model.Tweet_Rater()
    fname = 'textFiles/' + movieTitle + '.txt'
    f = open(fname, 'r', encoding='utf8')
    for tweet in f.readlines():
        # check if tweets still need to be collected
        cont = False
        for i in range(5):
            if len(tweets[i]) < 5:
                cont = True
        if cont == False:
            break

        if tweet == '\n':
            continue
        else:
            sent = rater.predict(tweet)
            
        if len(tweets[sent - 1]) < 5:
            tweets[sent - 1].append(tweet)
            
    print(tweets)

    # perform sentiment analysis
    output = subprocess.check_output(['python', 'sentAn.py', fname])
    score = float(output.decode('utf-8').split()[-1])
    print(score)

    # update database
    try:
        movies = pkl.load(open('./Database/database.pkl', 'rb'))
    except (EOFError, IOError):
        movies = {}

    try:
        top5 = pkl.load(open('./Database/top5.pkl', 'rb'))
    except (EOFError, IOError):
        top5 = []

    database = db.Database()
    database.movies = movies
    database.topFive = top5

    database.updateDatabase(movieTitle, score, tweets)
    
    pkl.dump(database.movies, open('./Database/database.pkl', 'wb'))
    pkl.dump(database.topFive, open('./Database/top5.pkl', 'wb'))

    # exit success
    sys.exit(0)
