# Socially Sensed Movie Analysis

### Manuel Marroquin, Hunter Mortemore, Andrew Nemecek
### Social Sensing / Cyber-Physical Systems, Spring 2019
### Dr. Dong Wang, Ph.D.


### Code Versioning

All code currently in the repository was written in Python 3.7 and utilizes the
Tweepy API to gather social media data from Twitter.

### Usage

To run the GUI, first open a terminal and run ./server.py.  Then, you may open the
./gui/gui.html page in a browser and begin searching for movies.  Note that, due to
the limitations of the Tweepy API, searching too many movies that are not in the
database too quickly will result in failed searches.