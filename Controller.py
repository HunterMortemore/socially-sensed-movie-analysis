#!/usr/local/anaconda3/bin/python3.7

import cherrypy
import json
import subprocess
import re
from decimal import Decimal

class Controller:

    def __init__(self):
        pass
    
    def GET_ALL(self, movie):
        output = {'result': 'success'}

        # Get the movie title from the request
        movie.replace(' ', '')

        # Handle getting the Twitter score
        databaseResult = subprocess.check_output(['python', 'getData.py', movie])
        score = float(databaseResult.decode('utf-8').splitlines()[0])
        if score < 0:
            subprocess.check_output(['python', 'TweetGatherer.py', movie])
            subprocess.check_output(['python', 'updateData.py', movie])
            databaseResult = subprocess.check_output(['python', 'getData.py', movie])
            score = float(databaseResult.decode('utf-8').splitlines()[0])
        remainingItems = databaseResult.decode('utf-8').splitlines()
        remainingItems = list(filter(None, remainingItems))
        output['score'] = format(score, '.2f')

        # Get the recommendations
        output['recommendations'] = {'r5': {'movie': re.sub(r"(?<=\w)([A-Z])", r" \1", remainingItems[-1].split(' ')[0]), 'score': format(float(remainingItems[-1].split(' ')[1]), '.2f')},
                                     'r4': {'movie': re.sub(r"(?<=\w)([A-Z])", r" \1", remainingItems[-2].split(' ')[0]), 'score': format(float(remainingItems[-2].split(' ')[1]), '.2f')},
                                     'r3': {'movie': re.sub(r"(?<=\w)([A-Z])", r" \1", remainingItems[-3].split(' ')[0]), 'score': format(float(remainingItems[-3].split(' ')[1]), '.2f')},
                                     'r2': {'movie': re.sub(r"(?<=\w)([A-Z])", r" \1", remainingItems[-4].split(' ')[0]), 'score': format(float(remainingItems[-4].split(' ')[1]), '.2f')},
                                     'r1': {'movie': re.sub(r"(?<=\w)([A-Z])", r" \1", remainingItems[-5].split(' ')[0]), 'score': format(float(remainingItems[-5].split(' ')[1]), '.2f')}}

        # Get the quoted tweets
        movie = re.sub(r"(?<=\w)([A-Z])", r" \1", movie)
        q1 = ['---','---','---','---','---']
        q2 = ['---','---','---','---','---']
        q3 = ['---','---','---','---','---']
        q4 = ['---','---','---','---','---']
        q5 = ['---','---','---','---','---']
        listInd = 1
        for i in range(1, 6):
            listInd += 1
            qInd = 0
            while (remainingItems[listInd].strip() != str(i + 1)) and (qInd != 5) and (remainingItems[listInd] != remainingItems[-5]):
                if i == 1:
                    q1[qInd] = remainingItems[listInd].replace('MOVIE TITLE', movie)
                elif i == 2:
                    q2[qInd] = remainingItems[listInd].replace('MOVIE TITLE', movie)
                elif i == 3:
                    q3[qInd] = remainingItems[listInd].replace('MOVIE TITLE', movie)
                elif i == 4:
                    q4[qInd] = remainingItems[listInd].replace('MOVIE TITLE', movie)
                elif i == 5:
                    q5[qInd] = remainingItems[listInd].replace('MOVIE TITLE', movie)
                qInd += 1
                listInd += 1
        
        tweetsScored1 = {'tweet1': q1[0], 'tweet2': q1[1], 'tweet3': q1[2], 'tweet4': q1[3], 'tweet5': q1[4]}
        tweetsScored2 = {'tweet1': q2[0], 'tweet2': q2[1], 'tweet3': q2[2], 'tweet4': q2[3], 'tweet5': q2[4]}
        tweetsScored3 = {'tweet1': q3[0], 'tweet2': q3[1], 'tweet3': q3[2], 'tweet4': q3[3], 'tweet5': q3[4]}
        tweetsScored4 = {'tweet1': q4[0], 'tweet2': q4[1], 'tweet3': q4[2], 'tweet4': q4[3], 'tweet5': q4[4]}
        tweetsScored5 = {'tweet1': q5[0], 'tweet2': q5[1], 'tweet3': q5[2], 'tweet4': q5[3], 'tweet5': q5[4]}

        output['quotes'] = {'1': tweetsScored1, '2': tweetsScored2, '3': tweetsScored3, '4': tweetsScored4, '5': tweetsScored5}

        # Return the output
        return json.dumps(output)