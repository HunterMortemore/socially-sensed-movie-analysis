#!/usr/local/anaconda3/bin/python3.7

import cherrypy
import Controller

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, OPTIONS"

if __name__ == "__main__":

    # CherryPy Configuration
    dispatcher = cherrypy.dispatch.RoutesDispatcher()
    conf = {'global': {'server.socket_host': 'localhost',
            'server.socket_port': 51000},
            '/': {'request.dispatch': dispatcher,
            'tools.CORS.on': True}
           }

    # CORS Setup
    cherrypy.tools.CORS = cherrypy.Tool('before_handler', CORS)

    # Controller Declaration
    masterController = Controller.Controller()

    # Dispatcher
    dispatcher.connect('getAll', '/all/:movie', controller = masterController, action = 'GET_ALL', conditions = dict(method = ['GET']))

    # CherryPy Activation
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)
